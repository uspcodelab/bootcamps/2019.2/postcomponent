import Event from './Event';

class CommentCreatedEvent extends Event {
  constructor(entity: any) {
    super('NewCommentCreated', 'new.comment', entity);
  }
}

export default CommentCreatedEvent;

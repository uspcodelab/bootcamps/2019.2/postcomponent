import Event from './Event';

class PostCreatedEvent extends Event {
  constructor(entity: any) {
    super('NewPostCreated', 'new.post', entity);
  }
}

export default PostCreatedEvent;

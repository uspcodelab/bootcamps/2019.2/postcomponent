import Event from './Event';

class CommentUpdatedEvent extends Event {
  constructor(entity: any) {
    super('CommentUpdated', `comment.${entity._id}`, entity);
  }
}

export default CommentUpdatedEvent;


import Event from './Event';

class PostUpdatedEvent extends Event {
  constructor(entity: any) {
    super('PostUpdated', `post.${entity._id}`, entity);
  }
}

export default PostUpdatedEvent;


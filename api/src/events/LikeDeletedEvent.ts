import Event from './Event';

class LikeDeletedEvent extends Event {
  constructor(entity: any) {
    super('LikeDeleted', 'delete.like', entity);
  }
}

export default LikeDeletedEvent;

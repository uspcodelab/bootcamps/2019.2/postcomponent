import Event from './Event';

class LikeCreatedEvent extends Event {
  constructor(entity: any) {
    super('NewLikeCreated', 'new.like', entity);
  }
}

export default LikeCreatedEvent;

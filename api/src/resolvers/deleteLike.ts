import { ApolloError } from 'apollo-server-koa';

import LikeDeletedEvent from '../events/LikeDeletedEvent';

async function deleteLike(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { Like }, stan }: any = context;
  const {
    _id,
  }: any = args;

  let like = await Like.findOne({
    _id,
  });

  if (!like) {
    throw new ApolloError('Like does not exist', '409');
  }

  const event = new LikeDeletedEvent(like);
  stan.publish(event.topic, event.serialized());

  return like;
}

export default deleteLike;

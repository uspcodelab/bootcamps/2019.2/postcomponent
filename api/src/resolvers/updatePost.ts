import { ApolloError } from 'apollo-server-koa';

import PostUpdatedEvent from 'src/events/PostUpdatedEvent';

async function updatePost(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { Post}, stan }: any = context;
  const {
    _id,
    data: {
      title,
      body,
      author,
      likes,
      timestamp,
    }
  }: any = args;

  let post = await Post.findOne({
    _id,
  });

  if (!post) {
    throw new ApolloError('Cannot find Post', '409');
  }

  post = {
    _id: post._id,
    title: title ? title : post.title,
    body: body ? body : post.body,
    author: author ? author : post.author,
    likes: likes ? likes : post.likes,
    timestamp: timestamp ? timestamp : post.timestamp,
  };

  const event = new PostUpdatedEvent(post);
  stan.publish(event.topic, event.serialized());

  return post;
}

export default updatePost;


import fs from 'fs';

const schema: string[] =
  fs.readFileSync('./src/graphql/schema.graphql')
  .toString()
  .split('\n');

function extractNames(type: string): string[] {
  const beginIndex: number =
    schema.findIndex((line: string) => line === `type ${type} {`);

  if (beginIndex < 0) {
    return [];
  }

  const endIndex: number =
    schema
    .slice(beginIndex)
    .findIndex((line: string) => line === '}') + beginIndex;

  const nameList: string[] = schema.slice(beginIndex + 1, endIndex);

  return nameList.map((name: string): string => {
    const match: RegExpMatchArray | null = name.match(/\b(\w+)\b/);

    if (match) {
      return match[0];
    }

    return '';
  });
}

const resolversNames: { [key: string]: string[] } = {
  Mutation: extractNames('Mutation'),
  Query: extractNames('Query'),
  Subscription: extractNames('Subscription'),
};

async function importFiles(rls: any, path: string, fileName: string): Promise<any> {
  if (!fileName.match(/.*\.ts$/)) {
    const newFiles: string[] =
      fs.readdirSync('./src/resolvers/' + path + fileName)
      .filter((fn: string) => fn !== 'index.ts');

    for (const fn of newFiles) {
      if (path) {
        await importFiles(rls, `${path}/${fileName}`, fn);
      } else {
        await importFiles(rls, fileName, fn);
      }
    }

    return rls;
  }

  const name: string = fileName.slice(0, fileName.length - 3);
  const resolver: any = {};
  resolver[name] = (await import(`${__dirname}/${path}/${name}`)).default;

  if (resolversNames.Mutation.includes(name)) {
    rls.Mutation = { ...rls.Mutation, ...resolver };
  } else if (resolversNames.Query.includes(name)) {
    rls.Query = { ...rls.Query, ...resolver };
  } else if (resolversNames.Subscription.includes(name)) {
    rls.Subscription = { ...rls.Subscription, ...resolver };
  }

  return rls;
}

const resolvers: any = {
  Mutation: {},
  Query: {},
  Subscription: {},
};

export default importFiles(resolvers, '', '');

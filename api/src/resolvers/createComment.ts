import { ApolloError } from 'apollo-server-koa';

import CommentCreatedEvent from '../events/CommentCreatedEvent';

async function createComment(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { Comment }, stan, uuidv4 }: any = context;
  const {
    data 
  }: any = args;

  let comment = await Comment.findOne({
    ...data 
  });

  if (comment) {
    throw new ApolloError('Comment already exists', '409');
  }

  comment = {
    _id: uuidv4(),
    ...data 
  };

  const event = new CommentCreatedEvent(comment);
  stan.publish(event.topic, event.serialized());

  return comment;
}

export default createComment;

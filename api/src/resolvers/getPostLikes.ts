import { ApolloError } from 'apollo-server-koa';

async function getPostLike(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { Like } }: any = context;
  const {
    post,
  }: any = args;

  let likes = await Like.find({
    post,
  });

  if (!likes) {
    throw new ApolloError('Likes not found', '409');
  }

  return likes;
}

export default getPostLike;

import { ApolloError } from 'apollo-server-koa';

import PostCreatedEvent from '../events/PostCreatedEvent';

async function createPost(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { Post }, stan, uuidv4 }: any = context;
  const {
    data 
  }: any = args;

  let post = await Post.findOne({
    ...data 
  });

  if (post) {
    throw new ApolloError('Post already exists', '409');
  }

  post = {
    _id: uuidv4(),
    ...data 
  };

  const event = new PostCreatedEvent(post);
  stan.publish(event.topic, event.serialized());

  return post;
}

export default createPost;

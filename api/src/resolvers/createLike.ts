import { ApolloError } from 'apollo-server-koa';

import LikeCreatedEvent from '../events/LikeCreatedEvent';

async function createLike(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { Like }, stan, uuidv4 }: any = context;
  const {
    data 
  }: any = args;

  let like = await Like.findOne({
    ...data 
  });

  if (like) {
    throw new ApolloError('Like already exists', '409');
  }

  like = {
    _id: uuidv4(),
    ...data 
  };

  const event = new LikeCreatedEvent(like);
  stan.publish(event.topic, event.serialized());

  return like;
}

export default createLike;

import { ApolloError } from 'apollo-server-koa';

async function getPost(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { Post } }: any = context;
  const {
    _id,
  }: any = args;

  let post = await Post.findOne({
    _id,
  });

  if (!post) {
    throw new ApolloError('Post not found', '409');
  }

  return post;
}

export default getPost;

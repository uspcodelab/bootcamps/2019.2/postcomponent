import { ApolloError } from 'apollo-server-koa';

import CommentUpdatedEvent from 'src/events/CommentUpdatedEvent';

async function updateComment(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { Comment}, stan }: any = context;
  const {
    _id,
    data: {
      title,
      body,
      author,
      likes,
      timestamp,
    }
  }: any = args;

  let comment = await Comment.findOne({
    _id,
  });

  if (!comment) {
    throw new ApolloError('Cannot find Comment', '409');
  }

  comment = {
    _id: comment._id,
    title: title ? title : comment.title,
    body: body ? body : comment.body,
    author: author ? author : comment.author,
    likes: likes ? likes : comment.likes,
    timestamp: timestamp ? timestamp : comment.timestamp, 
  };

  const event = new CommentUpdatedEvent(comment);
  stan.publish(event.topic, event.serialized());

  return comment;
}

export default updateComment;


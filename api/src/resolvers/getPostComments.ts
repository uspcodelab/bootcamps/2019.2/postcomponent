import { ApolloError } from 'apollo-server-koa';

async function getPostComments(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { Comment } }: any = context;
  const {
    post,
  }: any = args;

  let comments = await Comment.find({
    post,
  });

  if (!comments) {
    throw new ApolloError('Comments not found', '409');
  }

  return comments;
}

export default getPostComments;

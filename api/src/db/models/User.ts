import { Document, model, Model, Schema } from 'mongoose';


export interface User extends Document {
  _id: string;
  username: string;
}

export declare type TUser = Model<User>;

export const UserSchema: Schema = new Schema({
  _id: String,
  username: String,
}, {
  _id: false,
});

const User: TUser = model<User>('User', UserSchema);

export default User;


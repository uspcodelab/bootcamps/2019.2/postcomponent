import Handler from './Handler';

class LikeHandler extends Handler {
  constructor(stanContext: any, opts: any, db: any) {
    super(stanContext, opts, db, 'Like');
  }

  protected filterData(payload: any): any {
    return {
      _id: payload._id,
      author: payload.author,
      post: payload.post,
    };
  }

  protected listenUpdates(_id: string): void {
    const stan = this.stanContext.conn;

    const topic: string = `like.${_id}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload }: any = JSON
          .parse(msg.getData());
        
        await this.db[this.entityName]
          .updateOne({ _id }, payload);
      });

    this.stanContext.subs[topic]
      .on('unsubscribed', () => {
        console.log(`Unsubscribed from 'like.${_id}'`);
      });
  }

  protected cancelUpdates(_id: string): void {
    const topic: string = `like.${_id}`;
    this.stanContext.subs[topic].unsubscribe();
  }
}

export default LikeHandler;


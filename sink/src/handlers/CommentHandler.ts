import Handler from './Handler';

class CommentHandler extends Handler {
  constructor(stanContext: any, opts: any, db: any) {
    super(stanContext, opts, db, 'Comment');
  }

  protected filterData(payload: any): any {
    return {
      _id: payload._id,
      body: payload.body,
      author: payload.author,
      post: payload.post,
      likes: payload.likes,
      timestamp: payload.timestamp,
    };
  }

  protected listenUpdates(_id: string): void {
    const stan = this.stanContext.conn;

    const topic: string = `comment.${_id}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload }: any = JSON
          .parse(msg.getData());
        
        await this.db[this.entityName]
          .updateOne({ _id }, payload);
      });

    this.stanContext.subs[topic]
      .on('unsubscribed', () => {
        console.log(`Unsubscribed from 'comment.${_id}'`);
      });
  }

  protected cancelUpdates(_id: string): void {
    const topic: string = `comment.${_id}`;
    this.stanContext.subs[topic].unsubscribe();
  }
}

export default CommentHandler;


import Handler from './Handler';

class PostHandler extends Handler {
  constructor(stanContext: any, opts: any, db: any) {
    super(stanContext, opts, db, 'Post');
  }

  protected filterData(payload: any): any {
    return {
      _id: payload._id,
      title: payload.title,
      body: payload.body,
      author: payload.author,
      likes: payload.likes,
      timestamp: payload.timestamp,
    };
  }

  protected listenUpdates(_id: string): void {
    const stan = this.stanContext.conn;

    const topic: string = `post.${_id}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload }: any = JSON
          .parse(msg.getData());
        
        await this.db[this.entityName]
          .updateOne({ _id }, payload);
      });

    this.stanContext.subs[topic]
      .on('unsubscribed', () => {
        console.log(`Unsubscribed from 'post.${_id}'`);
      });
  }

  protected cancelUpdates(_id: string): void {
    const topic: string = `post.${_id}`;
    this.stanContext.subs[topic].unsubscribe();
  }
}

export default PostHandler;


import { Document, model, Model, Schema } from 'mongoose';


export interface Like extends Document {
  _id: string;
  author: string;
  post: string;
}

export declare type TLike = Model<Like>;

export const LikeSchema: Schema = new Schema({
  _id: String,
  author: String,
  post: String,
}, {
  _id: false,
});

const Like: TLike = model<Like>('Like', LikeSchema);

export default Like;


import { Document, model, Model, Schema } from 'mongoose';


export interface Comment extends Document {
  _id: string;
  body: string;
  author: string;
  post: string;
  likes: number;
  timestamp: string;
}

export declare type TComment = Model<Comment>;

export const CommentSchema: Schema = new Schema({
  _id: String,
  body: String,
  author: String,
  post: String,
  likes: Number,
  timestamp: String,
}, {
  _id: false,
});

const Comment: TComment = model<Comment>('Comment', CommentSchema);

export default Comment;

